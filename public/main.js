//event pada saat link di klik
$(".scroll").on("click", function(e){
    //ambil isi href
    var href = $(this).attr("href");
    //tangkap elemen ybs
    var elemen = $(href);
    
    //pindahkan scroll
    $("App").animate({
        scrollTop: elemen.offset().top - 50                                 
    }, 1250, 'easeInOutExpo');

    e.preventDefault();
});




// parallax
// about
$(window).on('load', function(){
    $('.kiri').addClass('pmuncul');
    $('.kanan').addClass('pmuncul');
})
$(window).scroll(function() {
    var wScroll = $(this).scrollTop();
    //jumbotron
    $('.jumbotron img').css({
        'transform' : 'translate(0px, '+ wScroll/4 +'%)'
     });

     $('.jumbotron h1').css({
        'transform' : 'translate(0px, '+ wScroll/2 +'%)'
     });

     $('.jumbotron p').css({
        'transform' : 'translate(0px, '+ wScroll/1.2 +'%)'
     });

     //gallery
     if(wScroll > $('.gallery').offset().top-250){
        $(".gallery .img-thumbnail").each(function(i){
            setTimeout(function(){
                $(".gallery .img-thumbnail").eq(i).addClass('muncul');
            }, 200 * (i+1));
        })
        
        
     }
      
});
