import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import About from './about/About';
import Footer from './footer/Footer';
import Gallery from './gallery/Gallery';
import Jumbotron from './jumbotron/Jumbotron';
import Navbar from './navbar/Navbar';
class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar/>
        <Jumbotron/>
        <About/>
        <Gallery/>
        <section class="contact" id="contact">
          <div className="container">
            <div className="row">
              <div className="col">
                <h2 className="text-center">Contact Us</h2>
                <hr/>
              </div>
            </div>

            <div className="row justify-content-center">
              <div className="col-md-4">
                <div className="card text-white mb-3 text-center">
                  <div className="card-body">
                    <h4 className="card-title">Contact Us</h4>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
                <div className="list-group">
                  <h1 href="#" className="list-group-item list-group-item-action">Location</h1>
                  <a href="#" className="list-group-item list-group-item-action">My School</a>
                  <a href="#" className="list-group-item list-group-item-action">Yogyakarta</a>
                  <a href="#" className="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                </div>
              </div>

              <div className="col-md-6">
                <form>
                  <div className="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" className="form-control" id="nama"/>
                  </div>
                  <div className="form-group">
                    <label for="email">Email</label>
                    <input type="text" className="form-control" id="email"/>
                  </div>
                  <div className="form-group">
                    <label for="tlp">No.Telphone</label>
                    <input type="text" className="form-control" id="tlp"/>
                  </div>
                  <div className="form-group">
                    <label for="pesan">Pesan</label>
                    <textarea name="pesan" className="form-control" id="pesan"></textarea>
                  </div>
                  <button className="btn btn-dark" type="submit">Kirim Pesan</button>
                </form>
              </div>
            </div>
          </div> 
        </section>
        <Footer/>
      </div>
    );
  }
}

export default App;
