import React, { Component } from 'react';
import './gallery.css'
import g1 from './bromo.jpg';
import g2 from './fatih.jpg';
import g3 from './gambar.jpg';
import g4 from './hp.jpg';
import g5 from './job.jpg';
import g6 from './pantai.jpg';

class Gallery extends Component {
    render() {
        return (
            <div>
                <section className="gallery" id="gallery">
                <div className="container">
                    <div className="row">
                    <div className="col-md-12 mb-4">
                        <h2 className="text-center">Gallery</h2>
                        <hr/>
                    </div>
                    </div>
                    <div className="row text-center"> 
                        <div className="col-md-4">
                            <a href="" className="img-thumbnail">
                            <img src={g1}/>
                            </a>
                        </div>
                        <div className="col-md-4">
                            <a href="" className="img-thumbnail">
                            <img src={g2}/>
                            </a>
                        </div>
                        <div className="col-md-4">
                            <a href="" className="img-thumbnail">
                            <img src={g3}/>
                            </a>
                        </div>
                    </div>
                        <br/>
                    <div className="row text-center">
                        <div className="col-md-4">
                            <a href="" className="img-thumbnail">
                            <img src={g4}/>
                            </a>
                        </div>
                        <div className="col-md-4">
                            <a href="" className="img-thumbnail">
                            <img src={g5}/>
                            </a>
                        </div>
                        <div className="col-md-4">
                            <a href="" className="img-thumbnail">
                            <img src={g6}/>
                            </a>
                        </div>
                    </div>
                </div>
                </section>
            </div>
        );
    }
}

export default Gallery;