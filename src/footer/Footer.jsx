import React, { Component } from 'react';
import './footer.css';


class Footer extends Component {
    render() {
        return (
            <div>
                <footer>
                <div className="container text-center">
                    <div className="row">
                    <div className="col-md-12">
                        <p>&copy; copyright 2018 | built by. Ibrahim Alfatih</p>
                    </div>
                    </div>
                </div>
                </footer>
            </div>
        );
    }
}

export default Footer;