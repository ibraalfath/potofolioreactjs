import React, { Component } from 'react';
import './navbar.css';


class Navbar extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark  fixed-top">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <a className="navbar-brand scroll" href="#jumbotron">Ibrahim Alfatih</a>
                
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item active">
                        <a className="nav-link scroll" href="#about">About Me<span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item active">
                        <a className="nav-link scroll" href="#gallery">Gallery<span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item active">
                        <a className="nav-link scroll" href="#contact">Contact<span className="sr-only">(current)</span></a>
                        </li>
                    </ul>
                    </div>
                </nav> 
            </div>
        );
    }
}

export default Navbar;