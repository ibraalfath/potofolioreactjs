import React, { Component } from 'react';
import fatih from './fatih.jpg';
import './jumbotron.css';

class Jumbotron extends Component {
    render() {
        return (
            <div>
                <div className="jumbotron text-center" id="jumbotron">
                <img src={fatih} className="rounded-circle"/>
                <h1>Ibrahim Alfatih</h1>
                <p>Web Design</p>
                </div>
            </div>
        );
    }
}

export default Jumbotron;