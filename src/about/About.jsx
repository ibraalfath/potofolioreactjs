import React, { Component } from 'react';
import './about.css'


class About extends Component {
    render() {
        return (
            <div>
                <section className="about" id="about">
                <div className="container">
                    <div className="row">
                    <div className="col-md-12 kiri mb-4">
                        <h2 className="text-center">About Me</h2>
                        <hr/>
                    </div>
                    </div>

                    <div className="row">
                    <div className="col-md-12">
                        <p className="kanan">&nbsp;&nbsp;Nama gw Ibrahim Alfatih atau panggil aja fatih. Gw lahir di Tegal tapi ga bisa bahasa Jawa 
                        karena di Tegal cuma numpang lahir aja :v, kalau besarnya sih di JawaBarat tepatnya yaitu di kota Bekasi. <br/>
                        &nbsp;&nbsp;Kalau gw sih hobynya noton film sama baca buku, Apalagi buku-buku yang berbau konspirasi  
                        pasti betah banget tuh duduk lama-lama buat baca buku, Dan kalau nonton filmnya sih gw lebih suka yang 
                        bergenre action. <br/> 
                        &nbsp;&nbsp;Dan kegiatan gw sekarang sih sebagai seorang Web Developer tepatnya di bagian Front-End tapi 
                        masih noob :v, Karena gw baru mulai belajar. <br/>
                        &nbsp;&nbsp;Adapun tempat gw mempelajari tentang Web Developer yaitu di Pondok IT tepatnya di Yogyakarta. Tidak seperti
                        kebanyakan pondok, Di Pondok IT ini selain kita mempelajari ilmu agama, kita juga mempelajari ilmu-ilmu teknologi,
                        seperti Web, Android, hingga Game Development.
                        </p>
                    </div>
                    </div>
                </div>
                </section>
            </div>
        );
    }
}

export default About;